#!/bin/bash

echo Database back up initiated...


SCRIPT=$(readlink -f backupCnf.config)

SCRIPTPATH=`dirname $SCRIPT`

. $SCRIPTPATH/conf/backupCnf.config


if [ -d "/home/Backup" ]
then
    echo "Directory /path/to/dir exists." 
else
    echo "Directory /path/to/dir does not exists. Creating Backup directory"
    mkdir /home/Backup
fi


mkdir /home/Backup/"Backup_""$(date +"%Y-%m-%d")"

cd /home/Backup/"Backup_""$(date +"%Y-%m-%d")"

echo Backup directory created...

docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3 > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/delta3.sql

docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3_data > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/delta3_data.sql

docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3_analytics > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/delta3_analytics.sql

docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  d3_test_datasets > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/d3_test_datasets.sql

docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  deltalytics  > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/deltalytics.sql

echo Back up process complete !!!
