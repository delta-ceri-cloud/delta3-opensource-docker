-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_event`
--

DROP TABLE IF EXISTS `d3_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_event` (
  `idEvent` int(11) NOT NULL,
  `description` text,
  `objectId` int(11) DEFAULT NULL,
  `objectType` varchar(45) DEFAULT NULL,
  `executeBy` int(11) DEFAULT NULL,
  `executeTS` timestamp NULL DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `Organization_idOrganization` int(11) DEFAULT NULL,
  `EventTemplate_idEventTemplate` int(11) NOT NULL,
  `Task_idTask` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEvent`),
  KEY `fk_Event_EventTemplae1_idx` (`EventTemplate_idEventTemplate`),
  CONSTRAINT `fk_Event_EventTemplae1` FOREIGN KEY (`EventTemplate_idEventTemplate`) REFERENCES `d3_eventtemplate` (`idEventTemplate`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_event`
--

LOCK TABLES `d3_event` WRITE;
/*!40000 ALTER TABLE `d3_event` DISABLE KEYS */;
INSERT INTO `d3_event` VALUES (1,'\"idConfiguration\":3,\"description\":\"Test DB\",\"active\":true,\"updatedTS\":\"2020-12-18 19:26:42.09\",\"type\":\"MySQL\",\"createdTS\":\"2020-12-18 19:26:42.09\",\"createdBy\":1,\"organization\":1,\"name\":\"My-SQL GCP\",\"dbUser\":\"CSUser\",\"connectionInfo\":\"jdbc:mysql://localhost:3306/d3_test_datasets?autoReconnect=true\",\"dbPassword\":\"VLnhrwpDdhc=\"',NULL,NULL,NULL,NULL,1,'2020-12-19 00:26:42',0,'2020-12-19 00:27:33',1,6,NULL),(2,'\"idConfiguration\":1,\"description\":\"Developemnt DB\",\"active\":true,\"updatedTS\":\"2020-12-18 19:26:55.421\",\"type\":\"MySQL\",\"createdTS\":\"2020-06-30 00:00:00.0\",\"createdBy\":15,\"organization\":1,\"name\":\"MySQL Local\",\"dbUser\":\"CSUser\",\"connectionInfo\":\"jdbc:mysql://db:3306/d3_test_datasets?autoReconnect=true\",\"dbPassword\":\"VLnhrwpDdhc=\"',NULL,NULL,NULL,NULL,1,'2020-12-19 00:26:55',0,'2020-12-19 00:27:33',1,4,NULL),(3,'\"idConfiguration\":1,\"description\":\"Developemnt DB\",\"active\":true,\"updatedTS\":\"2020-12-18 19:29:35.315\",\"type\":\"MySQL\",\"createdTS\":\"2020-06-30 00:00:00.0\",\"createdBy\":15,\"organization\":1,\"name\":\"MySQL Local\",\"dbUser\":\"CSUser\",\"connectionInfo\":\"jdbc:mysql://localhost:3306/d3_test_datasets?autoReconnect=true\",\"dbPassword\":\"VLnhrwpDdhc=\"',NULL,NULL,NULL,NULL,1,'2020-12-19 00:29:35',0,'2020-12-19 00:29:40',1,4,NULL),(4,'\"idConfiguration\":2,\"description\":\"Developemnt DB\",\"active\":true,\"updatedTS\":\"2020-12-20 00:55:06.533\",\"type\":\"MS-SQL\",\"createdTS\":\"2020-06-30 00:00:00.0\",\"createdBy\":15,\"organization\":1,\"name\":\"MS-SQL GCP\",\"dbUser\":\"sqlserver\",\"connectionInfo\":\"jdbc:sqlserver://35.203.110.135:1433;databaseName=Sample_MSSQL_test;\",\"dbPassword\":\"IrFt1BGktIg=\"',NULL,NULL,NULL,NULL,1,'2020-12-20 05:55:07',0,'2020-12-20 05:55:48',1,4,NULL);
/*!40000 ALTER TABLE `d3_event` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:45
