-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_eventtemplate`
--

DROP TABLE IF EXISTS `d3_eventtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_eventtemplate` (
  `idEventTemplate` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `actionClass` varchar(200) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `Module_idModule` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEventTemplate`),
  KEY `fk_EventTemplae_Module1_idx` (`Module_idModule`),
  CONSTRAINT `fk_EventTemplae_Module1` FOREIGN KEY (`Module_idModule`) REFERENCES `d3_module` (`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_eventtemplate`
--

LOCK TABLES `d3_eventtemplate` WRITE;
/*!40000 ALTER TABLE `d3_eventtemplate` DISABLE KEYS */;
INSERT INTO `d3_eventtemplate` VALUES (1,'Job Submit','Request submitted','1',NULL,1,1,'2014-09-15 17:39:41',1,'2014-09-15 17:39:41',6),(2,'Results Ready','Results received','1',NULL,1,1,'2014-09-15 17:52:52',1,'2014-09-15 17:52:52',6),(3,'Stat Method Update','Statistical method configuration was updated','1',NULL,1,1,'2014-12-23 17:00:00',1,'2014-12-23 17:00:00',1),(4,'Data Source Update','Data Source configuration was updated','1',NULL,1,1,'2014-12-23 17:00:00',1,'2014-12-23 17:00:00',1),(5,'Stat Method Created','Statistical method configuration was created','1',NULL,1,1,'2015-04-23 16:00:00',1,'2015-04-23 16:00:00',1),(6,'Data Source Created','Data Source configuration was created','1',NULL,1,1,'2015-04-23 16:00:00',1,'2015-04-23 16:00:00',1),(7,'Study Full Processing Start','Generate Flat Table and process study','1','com.ceri.delta.server.processor.ProcessStudyAll',1,6,'2015-08-11 04:00:00',1,'2020-07-21 18:41:09',6),(8,'Study Processing Start','Process study','1','com.ceri.delta.server.processor.ProcessStudy',1,1,'2015-08-13 04:00:00',1,'2020-07-21 18:41:19',6),(9,'Value Below Zero','Value of one of the results is below zero','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(10,'Value Above Primary CI','Value of one of the results is above primary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(11,'Value Above Secondary CI','Value of one of the results is above secondary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(12,'Value Below Primary CI','Value of one of the results is below primary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(13,'Value Below Secondary CI','Value of one of the results is below secondary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(14,'Trending Up','3 values of the results are increasing ','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(15,'Trending Down','3 values of the results are decreasing','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(16,'Stat Package Created','Statistical package configuration was created','1','',1,1,'2017-01-30 21:10:43',1,'2017-01-30 21:10:43',1),(17,'Stat Package Updated','Statistical Package configuration was updated','1','',1,1,'2017-01-30 21:11:40',1,'2017-01-30 21:11:40',1),(18,'Model Processing Start','Generate Flat Table','1','com.ceri.delta.server.processor.ProcessModel',1,1,'2020-10-05 04:00:00',1,'2020-10-08 03:54:34',6);
/*!40000 ALTER TABLE `d3_eventtemplate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:50
