-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_model`
--

DROP TABLE IF EXISTS `d3_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_model` (
  `idModel` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `idConfiguration` int(11) DEFAULT NULL,
  `outputName` varchar(45) DEFAULT NULL,
  `status` varchar(1024) DEFAULT NULL,
  `isAtomicFinished` tinyint(1) DEFAULT NULL,
  `isMissingFinished` tinyint(1) DEFAULT NULL,
  `isVirtualFinished` tinyint(1) DEFAULT NULL,
  `isSecondaryVirtualFinished` tinyint(1) DEFAULT NULL,
  `processingId` varchar(45) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `Organization_idOrganization` int(11) NOT NULL,
  `idGroup` int(11) DEFAULT '0',
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `originalModelId` int(11) DEFAULT NULL,
  `projectAssigned` int(11) DEFAULT NULL,
  PRIMARY KEY (`idModel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_model`
--

LOCK TABLES `d3_model` WRITE;
/*!40000 ALTER TABLE `d3_model` DISABLE KEYS */;
INSERT INTO `d3_model` VALUES (1,'Sample Local Test DB I','Sample Test Data on local MySQL',1,'ft1608337644989','Processing of Descriptive Statistics finished',1,1,1,1,'2020-12-19 00:16:12.989',0,0,1,0,1,'2020-12-18 05:00:00',1,'2020-12-20 05:57:50',0,0),(2,'Sample GCP Test DB 2','Sample Test Data on GCP MS-SQL',2,'ft1608444957174','New',0,0,0,0,NULL,0,0,1,0,1,'2020-12-20 05:00:00',1,'2020-12-20 06:17:38',0,0);
/*!40000 ALTER TABLE `d3_model` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:44
