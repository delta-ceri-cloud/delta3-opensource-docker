-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_permissiontemplate`
--

DROP TABLE IF EXISTS `d3_permissiontemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_permissiontemplate` (
  `idPermissionTemplate` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `Module_idModule` int(11) NOT NULL,
  PRIMARY KEY (`idPermissionTemplate`,`Module_idModule`),
  KEY `fk_PermissionTemplate_Module1_idx` (`Module_idModule`),
  CONSTRAINT `fk_PermissionTemplate_Module1` FOREIGN KEY (`Module_idModule`) REFERENCES `d3_module` (`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_permissiontemplate`
--

LOCK TABLES `d3_permissiontemplate` WRITE;
/*!40000 ALTER TABLE `d3_permissiontemplate` DISABLE KEYS */;
INSERT INTO `d3_permissiontemplate` VALUES (1,1,'2013-10-30 18:04:51',1,'2013-10-30 18:37:09','Admin Users','administer users','menu','Users',90,1,1),(2,1,'2013-10-30 18:38:39',1,'2013-10-30 18:43:57','Admin Roles','administer roles','menu','Roles',92,1,1),(3,1,'2013-10-30 18:41:16',1,'2013-10-30 18:44:08','Admin Groups','administer groups','menu','Groups',94,1,1),(4,1,'2013-10-30 18:43:34',1,'2013-10-30 18:44:15','Admin Organizations','administer organizations','menu','Organizations',96,1,1),(5,1,'2013-11-02 00:27:14',1,'2014-01-22 01:36:08','Results','user of statistical package','menu','Processes',30,1,2),(6,1,'2014-01-20 05:00:00',2,'2020-12-18 22:22:11','Data Models','access to Model Builder','menu','Models',5,1,2),(7,1,'2014-01-20 22:00:00',1,'2014-01-20 22:00:00','Permissions','administer permission templates','menu','Permissions',98,1,1),(8,1,'2014-03-12 20:00:00',1,'2014-06-11 01:22:38','Table Viewer','access to Table Viewer','button','TableViewer',1001,1,2),(9,1,'2014-06-30 20:00:00',1,'2014-06-30 20:00:00','Database Connections','program configuration','menu','Configurations',84,1,1),(10,1,'2014-07-03 20:00:00',1,'2014-07-03 20:00:00','Studies','tools to create Studies','menu','Studies',20,1,3),(11,1,'2017-01-31 20:00:00',6,'2017-01-31 20:00:00','Stat Method Configurator','statistical method configuration','button','Methods',86,1,1),(12,1,'2014-07-11 21:41:00',1,'2014-07-11 21:41:00','Filters','administer filters for studies','button','Filters',1002,1,3),(13,1,'2014-07-29 19:11:11',1,'2014-07-29 19:11:11','Delete Studies','allow deletion of studies','button','StudyDelete',1003,1,3),(14,1,'2014-08-01 04:00:00',2,'2020-12-18 22:22:41','Stored Formulas','create and maintain log regression formulas','menu','Logregrs',35,1,4),(15,6,'2014-09-08 20:42:38',6,'2014-09-08 20:42:38','Delete Processes','allow deletion of processes','button','ProcessDelete',1004,1,1),(16,6,'2014-09-12 04:00:00',2,'2020-12-18 22:26:06','Events','events, alerts, notifications','menu','Events',110,1,5),(17,6,'2014-09-15 04:00:00',2,'2020-12-18 22:26:28','Event Templates','administer event templates','menu','Eventtemplates',115,1,5),(18,6,'2014-09-15 04:00:00',2,'2020-12-18 22:28:02','Alerts','set up alerts, receive notifications','menu','Alerts',120,1,5),(19,6,'2014-09-17 04:00:00',2,'2020-12-18 22:28:59','Notifications','view and acknowledge notifications','menu','Notifications',125,1,5),(20,6,'2014-11-12 21:13:55',6,'2014-11-12 21:13:55','Delete Models','allow deletion of Models','button','ModelDelete',1005,1,1),(21,6,'2014-11-12 21:14:30',6,'2014-11-12 21:14:30','Pseudo','become another user','button','Pseudo',1006,1,1),(22,6,'2015-05-27 01:32:02',6,'2015-05-27 01:32:02','Lock Model','lock/unlock model','button','LockModel',1020,1,1),(23,6,'2015-08-07 17:37:39',6,'2015-08-07 17:37:39','Move Study','allow to move studies to projects','column','MoveStudy',1016,0,1),(24,6,'2015-08-07 17:38:46',6,'2015-08-07 17:38:46','Move Model','allow to move model to project','column','MoveModel',1017,0,1),(25,6,'2015-10-28 23:03:48',6,'2015-10-28 23:46:26','Web Services','execute web services','menu','Webservices',1500,1,1),(26,6,'2016-02-19 15:18:00',1,'2016-02-19 15:18:00','Health Survey','collect health and patient information ','menu','HealthSurvey',1600,1,7),(27,6,'2016-06-30 15:51:50',6,'2016-06-30 15:52:00','Audit','Allow access to audit information','menu','Audits',1010,1,1),(28,6,'2016-11-03 15:00:00',6,'2016-11-03 15:00:00','DELTAlytics','DELTA Analytics Module','menu','DAM',1510,1,1),(29,6,'2017-01-30 05:00:00',2,'2020-12-18 22:23:25','Statistics Configurator','statistical package configuration','menu','Stats',85,1,1),(30,1,'2017-03-15 14:00:00',1,'2017-03-15 14:00:00','Log Level','adjust logging cerver logging level','button','Logging',1030,1,1),(31,1,'2018-04-23 15:00:00',1,'2018-04-23 15:00:00','Chart Admin','administer chart reviews','app','ChartAdmin',1040,1,8),(32,1,'2018-04-23 15:00:00',1,'2018-04-23 15:00:00','Chart Reviewer','review ambulatory charts','app','ChartReviewer',1042,1,8),(33,1,'2018-06-28 13:46:00',1,'2018-06-28 13:46:00','Valueset Admin','administer valuesets','app','ValuesetAdmin',1044,1,9),(34,1,'2018-06-28 13:46:00',1,'2018-06-28 13:46:00','Valueset Reviewer','review valuesets','app','ValuesetReviewer',1046,1,9);
/*!40000 ALTER TABLE `d3_permissiontemplate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:47
