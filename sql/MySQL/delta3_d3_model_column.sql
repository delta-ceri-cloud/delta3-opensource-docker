-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_model_column`
--

DROP TABLE IF EXISTS `d3_model_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_model_column` (
  `idModelColumn` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `physicalName` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `defaultValue` varchar(1024) DEFAULT NULL,
  `defaultValueType` varchar(12) DEFAULT NULL,
  `keyField` tinyint(1) DEFAULT '0',
  `insertable` tinyint(1) DEFAULT '1',
  `atomic` tinyint(1) DEFAULT '1',
  `sub` tinyint(1) DEFAULT '0',
  `virtualField` tinyint(1) DEFAULT '0',
  `virtualOrder` tinyint(1) DEFAULT '0',
  `rollup` tinyint(4) DEFAULT NULL,
  `formula` varchar(1024) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `fieldClass` varchar(20) DEFAULT NULL,
  `fieldKind` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `ModelTable_idModelTable` int(11) DEFAULT NULL,
  `Model_idModel` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idModelColumn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_model_column`
--

LOCK TABLES `d3_model_column` WRITE;
/*!40000 ALTER TABLE `d3_model_column` DISABLE KEYS */;
INSERT INTO `d3_model_column` VALUES (1,'married','','married','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Continuous',1,1,1,1,'2020-12-19 05:00:00',1,'2020-12-19 05:14:26'),(2,'caseid','','caseid','INT(11)',NULL,NULL,1,1,1,0,0,0,0,'',0,'Case ID','Continuous',1,1,1,1,'2020-12-19 05:00:00',1,'2020-12-19 05:14:13'),(3,'ctrdate','','ctrdate','DATE',NULL,NULL,0,1,1,0,0,0,0,'',0,'Sequencer','Date',1,1,1,1,'2020-12-19 05:00:00',1,'2020-12-19 05:14:43');
/*!40000 ALTER TABLE `d3_model_column` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:40
